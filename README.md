# WHAT IS THIS?  

analysis script for paper 
'Sequential Replay of Non-spatial Task States in the Human Hippocampus'
by nicolas w schuck & yael niv, Science, 2019 

**code written by** nico schuck. write to nico dot schuck at gmail dot com  

script loads decoded sequences of states during rest, behavioral data, and on task decoding
plus some supporting variables 

# HOW CAN I RUN IT? 

1. clone the repo (in terminal): `git glone https:/gitlab/nschuck/fmri_state_replay`  
2. start a fresh R session in the cloned REPO FOLDER 
3. run `source('replay_paper_main_analyses.R')` in your R console 

### REQUIREMENTS 

**Operating System**: This script has been set up and tested on *Mac OS 10.14+* and Linux *Debian* and *Ubuntu (18.04.02)* distributions. Note that there can be differences between R package functionality in Windows based systems.  

**R version**: 3.5.3+

**Packages**:  
- lme4_1.1-21
- car_3.0-3
- Matrix_1.2-17 
- beeswarm_0.2.3
- plotrix_3.7-5
- igraph_1.2.4.1
- RColorBrewer_1.1-2

All packages will be loaded automatically 

**CAUTION**:
all packages called in the script 
not yet installed will be 
automatically installed from CRAN 

**Matrix Products**: 
It is best if BLAS and LAPACK are used as in Mac/Linux Systems (see sessionInfo below). 
If this is not the case, some of the complicated mixed effects models may fail to estimate some coefficients and produce different results. 
A remedy is to drop the estimates of the correlations between the nested effects by changing 
`(1 + FDIST_minz + RFREQ + ROI|ID\COL)` in line 922 
to 
`(1 + FDIST_minz + RFREQ + ROI|ID) + (1 + FDIST_minz + RFREQ + ROI||ID:COL)`


**See below** for example sessionInfo() output 

# OVERVIEW AND STRUCTURE 

steps 1-7 show how the data was processed and key statistics derived.
running them is necessary to prepare the data/plots done in steps 8 ff. 

key objects are 'HPC' and 'HPCperm', and most statistics are derived from these data.  
they contain the sequences of decoded states. the objetcs are organized in the following format: 
 
- each object is an array of lists
- each list element is itself an array containing data from one subj
- within the array, each column is one condition, and each row one decoded state. rows are sequential indexing time (the first is the first TR etc).  

the columns are:


1. pre task rest (called 'PRE' in paper)
2. rest at end of session 1 (called POST in paper)
3. rest at start of session 2 (called POST in paper)
4. rest at end of session 2 (called POST in paper)
5. instructions, 1st 100TRs, i.e. clipped/truncated if they were longer
6. POST rest matched in length to all instr TRs (from end of seesion 1 data)
7. decoded states during INSTR, only TRs before the task was trained (clipped)
8. POST rest matched in length to clipped instr TRs (again from end of seesion 1 data)


**NOTE**
for some analyses, all of the three POST conditions mentioned above 
are averaged, unless the compared-to condition has less data, see text.

arrays/objects that are not in the above list usually contain data from different ROIs. if so, ROIs are stored in the following order: 
1. Wholebrain  
2. OFC 
3. HPC
4. PPA_FFA
5. OFC_perm (permutation) 
6. HPC_perm (permutation) 

most other things are hopefully self explanatory 

# MORE ON SYSTEM SETUP 
Here is sessionInfo() output from Nico's system: 

```R
R version 3.6.0 (2019-04-26)
Platform: x86_64-apple-darwin15.6.0 (64-bit)
Running under: macOS Mojave 10.14.5


Matrix products: default
BLAS:   /Library/Frameworks/R.framework/Versions/3.6/Resources/lib/libRblas.0.dylib
LAPACK: /Library/Frameworks/R.framework/Versions/3.6/Resources/lib/libRlapack.dylib


Random number generation:
 RNG:     Mersenne-Twister
 Normal:  Inversion
 Sample:  Rounding


locale:
[1] en_US.UTF-8/en_US.UTF-8/en_US.UTF-8/C/en_US.UTF-8/en_US.UTF-8


attached base packages:
[1] stats     graphics  grDevices utils     datasets  methods   base


other attached packages:
[1] Rtsne_0.15         plotrix_3.7-5      beeswarm_0.2.3     car_3.0-2
[5] carData_3.0-2      lme4_1.1-21        Matrix_1.2-17      igraph_1.2.4.1
[9] RColorBrewer_1.1-2


loaded via a namespace (and not attached):
 [1] zip_2.0.1         Rcpp_1.0.1        compiler_3.6.0    pillar_1.3.1
 [5] cellranger_1.1.0  nloptr_1.2.1      forcats_0.4.0     tools_3.6.0
 [9] boot_1.3-22       tibble_2.1.1      nlme_3.1-139      lattice_0.20-38
[13] pkgconfig_2.0.2   rlang_0.3.4       openxlsx_4.1.0    curl_3.3
[17] haven_2.1.0       rio_0.5.16        hms_0.4.2         grid_3.6.0
[21] data.table_1.12.2 readxl_1.3.1      foreign_0.8-71    minqa_1.2.4
[25] magrittr_1.5      MASS_7.3-51.4     splines_3.6.0     abind_1.4-5
[29] crayon_1.3.4  

```

# LICENSE 
see LICENSE file for licensing information 
