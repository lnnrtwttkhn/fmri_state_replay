
se.bars <- function(x, y, se, col=1, wf=0, bi=TRUE, lwd = 2, horiz = FALSE) {
	
  # adds se bars to data x
  # this function is more general than it needs to be 
  # for the purposes of this script, but my be helpful in other cases 
	
  n = length(x)
	w = 0.02*max(x)*wf
	y = as.numeric(y)
	se = as.numeric(se)
  	col = rep(col, ceiling(n/length(col)))
  	if (horiz == FALSE) {
  		for (i in 1:n) {
  			segments(x[i]+w, y[i]+se[i], x[i]-w, y[i]+se[i], col=col[i], lwd = lwd)
  			if (bi == TRUE) {
  				segments(x[i], y[i]-se[i], x[i], y[i]+se[i], col=col[i], lwd = lwd)
  				segments(x[i]+w, y[i]-se[i], x[i]-w, y[i]-se[i], col=col[i], lwd = lwd)
  			} else {
  				segments(x[i], y[i], x[i], y[i]+se[i], col=col[i], lwd = lwd)
  			}
  		}
    } else {
    	for (i in 1:n) {
    		segments(x[i]+se[i], y[i]+w, x[i]+se[i], y[i]+w, col=col[i], lwd = lwd)
    		if (bi == TRUE) {
    			segments(x[i]-se[i], y[i], x[i]+se[i], y[i], col=col[i], lwd = lwd)
    			segments(x[i]-se[i], y[i]+w, x[i]-se[i], y[i]-w, col=col[i], lwd = lwd)
    		}  else {
    			segments(x[i], y[i], x[i]+se[i], y[i], col=col[i], lwd = lwd)
    		}
    	}
    }
}



std.error <- function(X, na.rm = TRUE, within = FALSE ) {
	
  # calculates standard errors of the mean 
	# colum-wise if matrix (assumes each row = 1 subj)
	# adjustment of within data 

	if (within == FALSE) {
		if (length(dim(X)) < 2) {
			n = length(X)
			ses = sd(X, na.rm = na.rm)/sqrt(n)	
		} else {
			ses = apply(X, 2, function(x) sd(x, na.rm = na.rm)/sqrt(length(x)))	
		}
	} else {
		n = dim(X)[1] # assuming subjects are rows 
		Y = X - matrix(rowMeans(X), dim(X)[1], dim(X)[2]) + mean(X) 
		ses = apply(Y, 2, function(x) sd(x, na.rm = na.rm)/sqrt(length(x)))
	}
	return(ses)
}

make.inds = function(k, y, se, mar=0.5, marT=0.5, lwd = 1, sl = 2, cex = 1, col = 1, offset = 0, horiz = FALSE, upside = 0) {
  	# plotting function to draw indicators signif diffs between data points. mostly for bar graphs  
  	# var 'sl' reflects significance "level" (I know, I know, there are no "levels" ... pls don't tell anyone)
	ctext = switch(sl+1, 'n.s.', '+', '*', '**', '***')

	k = as.matrix(k)
  	y = as.matrix(y)
  	se = as.matrix(se)
  	ncomparisons = dim(k)[which(dim(k) != 2)[1]]
  	upsidedown = upside*-2 + 1

  	if (!horiz) {
    	for (i in 1:ncomparisons) {
      		if (!upside) {
        		mi = which(y[,i]==max(y[,i]))[1]
        	} else {
        		mi = which(y[,i]==min(y[,i]))[1]
        	}
        	nmi = setdiff(c(1, 2), mi)
      		cy = y[mi, i] + (se[mi, i] + mar*se[mi, i])*upsidedown
      		segments(k[1, i] + offset, cy, k[2, i] - offset, cy, lwd = lwd, col = col)
      		segments(k[1, i] + offset, cy, k[1, i] + offset, y[1, i] + (se[1, i] + 0.5*se[1, i])*upsidedown, lwd = lwd, col = col)
      		segments(k[2 ,i] - offset, cy, k[2, i] - offset, y[2, i] + (se[2, i] + 0.5*se[2, i])*upsidedown, lwd = lwd, col = col)
      		text(mean(k[,i]), y=cy+(marT*se[mi, i])*upsidedown, labels=ctext, cex = cex, col = col)
    	}
  	} else {
    	for (i in 1:ncomparisons) {
      		if (!upside) {
        		mi = which(y[,i]==max(y[,i]))[1]
        	} else {
        		mi = which(y[,i]==min(y[,i]))[1]
        	}
      		nmi = setdiff(c(1, 2), mi)
      		cy = y[mi, i] + (se[mi, i] + mar*se[mi, i])*upsidedown
      		segments(cy, k[1, i] + offset, cy, k[2, i] - offset, lwd = lwd, col = col)
      		segments(cy, k[1, i] + offset,  y[1, i] + (se[1, i] + 0.5*mar*se[1, i])*upsidedown, k[1, i] + offset, lwd = lwd, col = col)
      		segments(cy, k[2, i] - offset, y[2, i] + (se[2, i] + 0.5*mar*se[2, i])*upsidedown, k[2 ,i] - offset, lwd = lwd, col = col)
      		text(cy+(marT*se[mi, i])*upsidedown, y=mean(k[,i]), labels=ctext, cex = cex, col = col)
    	}
  	}
}